#include "time.h"

#include "data_manager/interface.h"

#define TEST_TABLE_NAME "test"
#define TEST1_TABLE_NAME "test1"

typedef struct TestResult
{
    uint32_t elements_count;
    float time;
} TestResult;

uint8_t test_predicate(CellValue left_value, CellValue right_value);
uint8_t predicate_bool_equal(CellValue left_value, CellValue right_value);
uint8_t predicate_float_greater(CellValue left_value, CellValue right_value);
CellValue test_transformation(CellValue cell_value);

ColumnSchema_LinkedList test_table_schema(void);
void ** random_test_table_row_get(void);
void test_table_row_free(void **row);

TestResult test_table_insert(State *state, Table *table, uint16_t count);
TestResult test_table_delete(State *state, Table *table, float value);

void test_500_insert_400_delete(char *storage_file_name, char *insert_results_file_name, char *delete_results_file_name);
void test_general(char *storage_file_name);



ColumnSchema_LinkedList test_table_schema(void)
{
    ColumnSchema_LinkedList schema = ColumnSchema_linked_list_new();
    ColumnSchema_linked_list_push_back(&schema, column_schema_new("test_bool", BOOL));
    ColumnSchema_linked_list_push_back(&schema, column_schema_new("test_uint8", UINT_8));
    ColumnSchema_linked_list_push_back(&schema, column_schema_new("test_uint16", UINT_16));
    ColumnSchema_linked_list_push_back(&schema, column_schema_new("test_uint32", UINT_32));
    ColumnSchema_linked_list_push_back(&schema, column_schema_new("test_int32", INT_32));
    ColumnSchema_linked_list_push_back(&schema, column_schema_new("test_float", FLOAT));
    return schema;
}

void ** random_test_table_row_get(void)
{
    uint8_t test_bool = 0;
    if (((float) rand() / RAND_MAX) > 0.5) test_bool = 1;
    uint8_t test_uint8 = (uint8_t) (((float) rand() / RAND_MAX) * UINT8_MAX);
    uint16_t test_uint16 = (uint16_t) (((float) rand() / RAND_MAX) * UINT16_MAX);
    uint32_t test_uint32 = (uint32_t) (((float) rand() / RAND_MAX) * UINT32_MAX);
    int32_t test_int32 = (int32_t) ((((float) rand() / RAND_MAX) - 0.5) * INT32_MAX);
    float test_float = (float) ((float) rand() / RAND_MAX);

    void **row = malloc(sizeof(void *) * 6);
    row[0] = malloc(sizeof(uint8_t));
    row[1] = malloc(sizeof(uint8_t));
    row[2] = malloc(sizeof(uint16_t));
    row[3] = malloc(sizeof(uint32_t));
    row[4] = malloc(sizeof(int32_t));
    row[5] = malloc(sizeof(float));
    *((uint8_t *) row[0]) = test_bool;
    *((uint8_t *) row[1]) = test_uint8;
    *((uint16_t *) row[2]) = test_uint16;
    *((uint32_t *) row[3]) = test_uint32;
    *((int32_t *) row[4]) = test_int32;
    *((float *) row[5]) = test_float;

    return row;
}

void test_table_row_free(void **row)
{
    free(row[0]);
    free(row[1]);
    free(row[2]);
    free(row[3]);
    free(row[4]);
    free(row[5]);
    free(row);
}



uint8_t test_predicate(CellValue left_value, __attribute__((unused)) CellValue right_value)
{
    return (uint8_t) (((float) rand() / RAND_MAX) < left_value._float);
}

uint8_t predicate_bool_equal(CellValue left_value, CellValue right_value)
{
    return left_value.bool == right_value.bool;
}

uint8_t predicate_float_greater(CellValue left_value, CellValue right_value)
{
    return left_value._float > right_value._float;
}

CellValue test_transformation(CellValue cell_value)
{
    float new_value = cell_value._float + 1;
    return TRY(CellValue, cell_value_new(FLOAT, &new_value));
}



TestResult test_table_insert(State *state, Table *table, uint16_t count)
{
    Query insert_query = insert_query_new();
    insert_query_set_table(&insert_query, table);
    for (uint16_t i=0; i<count; i++)
    {
        void **data = random_test_table_row_get();
        insert_query_add_row(&insert_query, row_new_by_values(&table->schema, data));
        test_table_row_free(data);
    }
    clock_t begin = clock();
    uint32_t inserted_count = query_execute(state, &insert_query);
    clock_t end = clock();
    query_free(&insert_query);
    return (TestResult) { .time = (float) (end - begin) / CLOCKS_PER_SEC, .elements_count = inserted_count };
}

TestResult test_table_delete(State *state, Table *table, float value)
{
    Query delete_query = delete_query_new();
    delete_query_set_table(&delete_query, table);
    Predicate predicate = predicate_new(test_predicate, operand_new_by_value(TRY(CellValue, cell_value_new(FLOAT, &value))), operand_new_by_value(TRY(CellValue, cell_value_new(FLOAT, &value))));
    delete_query_add_predicate(&delete_query, predicate);
    clock_t begin = clock();
    uint32_t deleted_count = query_execute(state, &delete_query);
    clock_t end = clock();
    query_free(&delete_query);
    return (TestResult) { .time = (float) (end - begin) / CLOCKS_PER_SEC, .elements_count = deleted_count };
}

void test_500_insert_400_delete(char *storage_file_name, char *insert_results_file_name, char *delete_results_file_name)
{
    srand(time(NULL));

    State state = storage_connect(storage_file_name);

    ColumnSchema_LinkedList schema = test_table_schema();
    Table *table = table_open(&state, TEST_TABLE_NAME);
    if (table == 0) table = table_create(&state, TEST_TABLE_NAME, &schema);

    FILE *insert_results_file = fopen(insert_results_file_name, "w+");
    FILE *delete_results_file = fopen(delete_results_file_name, "w+");
    uint32_t elements_count = 0;

    for (uint16_t i=1; i<5000; i++)
    {
        for (uint16_t j=0; j<5; j++)
        {
            TestResult insert_result = test_table_insert(&state, table, 100);
            fprintf(insert_results_file, "%"PRIu32",%"PRIu32",%f\n", elements_count, insert_result.elements_count, insert_result.time);
            elements_count += 100;
        }

        for (uint16_t j=0; j<4; j++)
        {
            TestResult delete_result = test_table_delete(&state, table, 100 / (float) elements_count);
            fprintf(delete_results_file, "%"PRIu32",%"PRIu32",%f\n", elements_count, delete_result.elements_count, delete_result.time);
            elements_count -= delete_result.elements_count;
        }

        printf("%"PRIu16"\n", i);
    }

    fclose(insert_results_file);
    fclose(delete_results_file);

    storage_disconnect(&state);
}



void test_general(char *storage_file_name)
{
    srand(time(NULL));

    State state = storage_connect(storage_file_name);

    printf("create or open TEST table\n");
    ColumnSchema_LinkedList schema = test_table_schema();
    Table *table = table_open(&state, TEST_TABLE_NAME);
    if (table == 0) table = table_create(&state, TEST_TABLE_NAME, &schema);

    printf("\ninsert into TEST 20 random rows\n");
    Query insert_query = insert_query_new();
    insert_query_set_table(&insert_query, table);
    for (uint16_t i=0; i<20; i++)
    {
        void **data = random_test_table_row_get();
        insert_query_add_row(&insert_query, row_new_by_values(&table->schema, data));
        test_table_row_free(data);
    }
    query_execute(&state, &insert_query);
    query_free(&insert_query);

    printf("\nselect * from TEST:\n");
    Query select_query = select_query_new();
    DataIterator it = data_iterator_table_new(&state, table->id);
    select_query_set_data_iterator(&select_query, &it);
    query_execute(&state, &select_query);
    query_free(&select_query);

    printf("\nselect * from TEST where test_bool == true:\n");
    select_query = select_query_new();
    it = data_iterator_table_new(&state, table->id);
    select_query_set_data_iterator(&select_query, &it);
    uint8_t true_bool_value = 1;
    select_query_add_predicate(&select_query, predicate_new(&predicate_bool_equal, operand_new_by_ptr(BOOL, "test_bool"), operand_new_by_value(TRY(CellValue, cell_value_new(BOOL, &true_bool_value)))));
    query_execute(&state, &select_query);
    query_free(&select_query);

    printf("\ncreate or open TEST1 table\n");
    Table *table1 = table_open(&state, TEST1_TABLE_NAME);
    if (table1 == 0) table1 = table_create(&state, TEST1_TABLE_NAME, &schema);

    printf("\ninsert into TEST1 20 random rows\n");
    insert_query = insert_query_new();
    insert_query_set_table(&insert_query, table1);
    for (uint16_t i=0; i<20; i++)
    {
        void **data = random_test_table_row_get();
        insert_query_add_row(&insert_query, row_new_by_values(&table1->schema, data));
        test_table_row_free(data);
    }
    query_execute(&state, &insert_query);
    query_free(&insert_query);

    printf("\nselect * from TEST1:\n");
    select_query = select_query_new();
    it = data_iterator_table_new(&state, table1->id);
    select_query_set_data_iterator(&select_query, &it);
    query_execute(&state, &select_query);
    query_free(&select_query);

    printf("\nselect * from TEST join TEST1 on test_bool == test1.test_bool where test1.test_float > 0.5:\n");
    select_query = select_query_new();
    it = data_iterator_table_new(&state, table->id);
    DataIterator join_it = data_iterator_join_new(&state, &it, table1->id);
    select_query_set_data_iterator(&select_query, &join_it);
    float float_05_value = 0.5;
    select_query_add_predicate(&select_query, predicate_new(&predicate_float_greater, operand_new_by_ptr(FLOAT, "test1.test_float"), operand_new_by_value(TRY(CellValue, cell_value_new(FLOAT, &float_05_value)))));
    query_execute(&state, &select_query);
    query_free(&select_query);

    printf("\ndelete from TEST1 where test_float > 0.5\n");
    Query delete_query = delete_query_new();
    delete_query_set_table(&delete_query, table1);
    delete_query_add_predicate(&delete_query, predicate_new(&predicate_float_greater, operand_new_by_ptr(FLOAT, "test_float"), operand_new_by_value(TRY(CellValue, cell_value_new(FLOAT, &float_05_value)))));
    query_execute(&state, &delete_query);
    query_free(&delete_query);

    printf("\nselect * from TEST1:\n");
    select_query = select_query_new();
    it = data_iterator_table_new(&state, table1->id);
    select_query_set_data_iterator(&select_query, &it);
    query_execute(&state, &select_query);
    query_free(&select_query);
    
    printf("\nupdate TEST1 set test_float += 1\n");
    Query update_query = update_query_new();
    update_query_set_table(&update_query, table1);
    update_query_add_transformation(&update_query, transformation_new(arg_ptr_new(FLOAT, "test_float"), &test_transformation));
    query_execute(&state, &update_query);
    query_free(&update_query);

    printf("\nselect * from TEST1:\n");
    select_query = select_query_new();
    it = data_iterator_table_new(&state, table1->id);
    select_query_set_data_iterator(&select_query, &it);
    query_execute(&state, &select_query);
    query_free(&select_query);
    
    storage_disconnect(&state);
}


int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printf("usage: lab1 [mode (1 -- 500 insert 400 delete test; 2 -- general tests)]\nlab1 1 [storage_file_name] [insert_results_file_name] [delete_results_file_name]\nlab1 2 [storage_file_name]\n");
        exit(1);
    }
    if (argv[1][0] == '1')
    {
        if (argc != 5)
        {
            printf("usage: lab1 [mode (1 -- 500 insert 400 delete test; 2 -- general tests)]\nlab1 1 [storage_file_name] [insert_results_file_name] [delete_results_file_name]\nlab1 2 [storage_file_name]\n");
            exit(1);
        }
        test_500_insert_400_delete(argv[2], argv[3], argv[4]);
    }
    else if (argv[1][0] == '2')
    {
        if (argc != 3)
        {
            printf("usage: lab1 [mode (1 -- 500 insert 400 delete test; 2 -- general tests)]\nlab1 1 [storage_file_name] [insert_results_file_name] [delete_results_file_name]\nlab1 2 [storage_file_name]\n");
            exit(1);
        }
        test_general(argv[2]);
    }
    else
    {
        printf("usage: lab1 [mode (1 -- 500 insert 400 delete test; 2 -- general tests)]\nlab1 1 [storage_file_name] [insert_results_file_name] [delete_results_file_name]\nlab1 2 [storage_file_name]\n");
        exit(1);
    }
}