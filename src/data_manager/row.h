#ifndef ROW_H_
#define ROW_H_

#include "cell.h"

typedef struct Row
{
    ColumnSchema_LinkedList schema;
    Cell_LinkedList cells;
} Row;

LINKED_LIST_HEADER(Row);


Row row_new(ColumnSchema_LinkedList);
Row row_new_by_values(ColumnSchema_LinkedList *, void **values);
Maybe_Nothing row_join_row(Row *row, Row *to_join, char *row_table_name);
void row_free(Row *);
void row_print(Row *row);

Maybe_Nothing row_set_cells(Row *row, Cell_LinkedList);
Maybe_Cell row_get_cell_by_name(Row *, char *);
Maybe_Nothing row_set_cell_by_name(Row *, Cell, char *);

Row_LinkedList rows_new(void);
void rows_add_row(Row_LinkedList *rows, Row row);
void rows_free(Row_LinkedList *rows);

#endif