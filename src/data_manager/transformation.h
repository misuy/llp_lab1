#ifndef TRANSFORMATION_H_
#define TRANSFORMATION_H_

#include "predicate.h"

typedef struct Transformation
{
    CellValuePtr arg_ptr;
    CellValue (*transform) (CellValue);
} Transformation;

LINKED_LIST_HEADER(Transformation);



CellValuePtr arg_ptr_new(ColumnType type, char *name);

Transformation transformation_new(CellValuePtr arg_ptr, CellValue (*transform) (CellValue));
Transformation_LinkedList transformations_new(void);
void transformations_add_transformation(Transformation_LinkedList *transformations, Transformation transformation);
void transformations_free(Transformation_LinkedList *transformations);
Maybe_Nothing transformation_apply(Transformation *transformation, Row *row);
Maybe_Nothing transformations_apply(Transformation_LinkedList *transformations, Row *row);

#endif