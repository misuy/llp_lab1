#include "interface.h"


Table* table_open(State *state, char *name)
{
    return TRY(TablePtr, root_table_get_table_by_name(state, name));
}

Table* table_create(State *state, char *name, ColumnSchema_LinkedList *schema)
{
    uint16_t id = TRY(uint16_t, get_id_for_new_table(state));
    Table* table = table_new(id, name, 0, !column_schema_is_variable_size(schema), *schema, column_schema_get_size(schema), 0, 0);
    state->tables[id] = table;
    TRY(Nothing, table_update(state, id));
    TRY(Nothing, state_sync_tables(state));
    return table;
}

void table_close(State *state, Table *table)
{
    table_free(state, table->id);
}


State storage_connect(char *name)
{
    return TRY(State, state_init(name));
}

void storage_disconnect(State *state)
{
    state_sync_tables(state);
    file_close(state->file);
    for (uint16_t i=0; i<MAX_TABLES_COUNT; i++)
    {
        if (state->tables[i] != 0) free(state->tables[i]);
    }
    free(state->file);
}