#include "query.h"

void select_query_free(Query *query);
void insert_query_free(Query *query);
void delete_query_free(Query *query);
void update_query_free(Query *query);


uint32_t select_query_execute(State *state, Query *query)
{
    if (query->type != SELECT)
    {
        printf("different query type\n");
        exit(1);
    }

    uint32_t selected_count = 0;
    while (!data_iterator_is_null(query->select.it))
    {
        Row row = data_iterator_get_row(query->select.it);
        if (TRY(uint8_t, predicates_apply(&row, &query->select.predicates)))
        {
            selected_count++;
            row_print(&row);
            printf("\n");
        }
        data_iterator_next(state, query->select.it);
    }
    data_iterator_free(query->select.it);
    return selected_count;
}

uint32_t insert_query_execute(State *state, Query *query)
{
    if (query->type != INSERT)
    {
        printf("different query type\n");
        exit(1);
    }

    uint32_t inserted_count = 0;
    Row_LinkedListNode *it = query->insert.rows.head;
    while (it != NULL)
    {
        //printf("insert\n");
        SlotPtr free_slot_ptr = TRY(SlotPtr, table_find_free_slot(state, query->insert.table->id));
        TRY(Nothing, table_write_row(state, &it->value, free_slot_ptr));
        inserted_count++;
        it = it->next;
    }
    return inserted_count;
}

uint32_t delete_query_execute(State *state, Query *query)
{
    if (query->type != DELETE)
    {
        printf("different query type\n");
        exit(1);
    }

    SlotPtr_LinkedList to_delete = SlotPtr_linked_list_new();
    RowIterator it = TRY(RowIterator, row_iterator_new(state, query->delete.table->id));
    while (!it.is_null)
    {
        if (TRY(uint8_t, predicates_apply(&it.row, &query->delete.predicates)))
            SlotPtr_linked_list_push_back(&to_delete, it.slot_iterator.ptr);
        TRY(Nothing, row_iterator_next(state, &it));
    }
    row_iterator_free(&it);

    uint32_t deleted_count = 0;
    SlotPtr_LinkedListNode *to_delete_it = to_delete.head;
    while (to_delete_it != NULL)
    {
        TRY(Nothing, table_delete_row(state, to_delete_it->value));
        deleted_count++;
        to_delete_it = to_delete_it->next;
    }
    SlotPtr_linked_list_free(&to_delete);
    return deleted_count;
}

uint32_t update_query_execute(State *state, Query *query)
{
    if (query->type != UPDATE)
    {
        printf("different query type\n");
        exit(1);
    }

    SlotPtr_LinkedList to_update = SlotPtr_linked_list_new();
    RowIterator it = TRY(RowIterator, row_iterator_new(state, query->update.table->id));
    while (!it.is_null)
    {
        if (TRY(uint8_t, predicates_apply(&it.row, &query->update.predicates)))
        {
            SlotPtr_linked_list_push_back(&to_update, it.slot_iterator.ptr);
        }
        TRY(Nothing, row_iterator_next(state, &it));
    }
    row_iterator_free(&it);

    uint32_t updated_count = 0;
    SlotPtr_LinkedListNode *to_update_it = to_update.head;
    while (to_update_it != NULL)
    {
        Row row = TRY(Row, table_read_row(state, to_update_it->value));
        TRY(Nothing, table_delete_row(state, to_update_it->value));
        TRY(Nothing, transformations_apply(&query->update.transformations, &row));
        SlotPtr free_ptr = TRY(SlotPtr, table_find_free_slot(state, query->update.table->id));
        TRY(Nothing, table_write_row(state, &row, free_ptr));
        row_free(&row);
        updated_count++;
        to_update_it = to_update_it->next;
    }
    SlotPtr_linked_list_free(&to_update);
    return updated_count;
}

uint32_t query_execute(State *state, Query *query)
{
    uint32_t result = 0;
    switch (query->type)
    {
        case SELECT:
            result = select_query_execute(state, query);
            break;
        case INSERT:
            result = insert_query_execute(state, query);
            break;
        case DELETE:
            result = delete_query_execute(state, query);
            break;
        case UPDATE:
            result = update_query_execute(state, query);
            break;
    }

    state_sync_tables(state);
    return result;
}


Query query_new(QueryType type)
{
    return (Query) { .type = type };
}

Query select_query_new(void)
{
    Query query = query_new(SELECT);
    query.select.predicates = predicates_new();
    return query;
}

void select_query_set_data_iterator(Query *query, DataIterator *it)
{
    query->select.it = it;
}

void select_query_set_predicates(Query *query, Predicate_LinkedList predicates)
{
    predicates_free(&query->select.predicates);
    query->select.predicates = predicates;
}

void select_query_add_predicate(Query *query, Predicate predicate)
{
    predicates_add_predicate(&query->select.predicates, predicate);
}

void select_query_free(Query *query)
{
    data_iterator_free(query->select.it);
    predicates_free(&query->select.predicates);
}


Query insert_query_new(void)
{
    Query query = query_new(INSERT);
    query.insert.rows = rows_new();
    return query;
}

void insert_query_set_table(Query *query, Table *table)
{
    query->insert.table = table;
}

void insert_query_add_row(Query *query, Row row)
{
    rows_add_row(&query->insert.rows, row);
}

void insert_query_set_rows(Query *query, Row_LinkedList rows)
{
    rows_free(&query->insert.rows);
    query->insert.rows = rows;
}

void insert_query_free(Query *query)
{
    rows_free(&query->insert.rows);
}


Query delete_query_new(void)
{
    Query query = query_new(DELETE);
    query.delete.predicates = predicates_new();
    return query;
}

void delete_query_set_table(Query *query, Table *table)
{
    query->delete.table = table;
}

void delete_query_set_predicates(Query *query, Predicate_LinkedList predicates)
{
    predicates_free(&query->delete.predicates);
    query->delete.predicates = predicates;
}

void delete_query_add_predicate(Query *query, Predicate predicate)
{
    predicates_add_predicate(&query->delete.predicates, predicate);
}

void delete_query_free(Query *query)
{
    predicates_free(&query->delete.predicates);
}


Query update_query_new(void)
{
    Query query = query_new(UPDATE);
    query.update.predicates = predicates_new();
    query.update.transformations = transformations_new();
    return query;
}

void update_query_set_table(Query *query, Table *table)
{
    query->update.table = table;
}

void update_query_set_predicates(Query *query, Predicate_LinkedList predicates)
{
    predicates_free(&query->update.predicates);
    query->update.predicates = predicates;
}

void update_query_add_predicate(Query *query, Predicate predicate)
{
    predicates_add_predicate(&query->update.predicates, predicate);
}

void update_query_set_transformations(Query *query, Transformation_LinkedList transformations)
{
    transformations_free(&query->update.transformations);
    query->update.transformations = transformations;
}

void update_query_add_transformation(Query *query, Transformation transformation)
{
    transformations_add_transformation(&query->update.transformations, transformation);
}

void update_query_free(Query *query)
{
    predicates_free(&query->update.predicates);
    transformations_free(&query->update.transformations);
}


void query_free(Query *query)
{
    switch (query->type)
    {
        case SELECT:
            select_query_free(query);
            break;
        case INSERT:
            insert_query_free(query);
            break;
        case DELETE:
            delete_query_free(query);
            break;
        case UPDATE:
            update_query_free(query);
            break;
    }
}