#ifndef PREDICATE_H_
#define PREDICATE_H_

#include "inttypes.h"

#include "util.h"
#include "cell.h"
#include "row.h"


typedef enum Operator
{
    EQUAL,
    NOT_EQUAL,
    LESS,
    NOT_LESS,
    GREATER,
    NOT_GREATER,
} Operator;


typedef struct CellValuePtr
{
    ColumnType type;
    char *name;
} CellValuePtr;


typedef struct Operand
{
    uint8_t is_loaded;
    union
    {
        CellValue value;
        CellValuePtr value_ptr;
    };
} Operand;


typedef struct Predicate
{
    uint8_t (*compare) (CellValue, CellValue);
    Operand left_operand;
    Operand right_operand;
} Predicate;

LINKED_LIST_HEADER(Predicate)



Operand operand_new_by_value(CellValue);
Operand operand_new_by_ptr(ColumnType, char *);

Predicate predicate_new(uint8_t (*compare) (CellValue, CellValue), Operand left, Operand right);
void predicate_free(Predicate *predicate);

Maybe_uint8_t predicate_apply(Row *, Predicate);

Predicate_LinkedList predicates_new(void);
void predicates_add_predicate(Predicate_LinkedList *, Predicate);
Maybe_uint8_t predicates_apply(Row *, Predicate_LinkedList *);
void predicates_free(Predicate_LinkedList *);

#endif