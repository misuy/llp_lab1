#include "predicate.h"

void operand_free(Operand *operand);
Maybe_Nothing operand_load(Row *row, Operand *operand);


LINKED_LIST(Predicate)


Operand operand_new_by_value(CellValue value)
{
    return (Operand) { .is_loaded = 1, .value = value };
}

Operand operand_new_by_ptr(ColumnType type, char *name)
{
    return (Operand) { .is_loaded = 0, .value_ptr = (CellValuePtr) { .type = type, .name = name } };
}

void operand_free(Operand *operand)
{
    if (operand->is_loaded) cell_value_free(&operand->value);
}


Predicate predicate_new(uint8_t (*compare) (CellValue, CellValue), Operand left, Operand right)
{
    return (Predicate) { .compare = compare, .left_operand = left, .right_operand = right };
}

void predicate_free(Predicate *predicate)
{
    operand_free(&predicate->left_operand);
    operand_free(&predicate->right_operand);
}


Maybe_Nothing operand_load(Row *row, Operand *operand)
{
    if (operand->is_loaded) OK(Nothing);
    Cell cell = HANDLE(Nothing, Cell, row_get_cell_by_name(row, operand->value_ptr.name));
    if (cell.type != operand->value_ptr.type) EXCEPTION(Nothing, "operand and cell types does not match");
    operand->value = HANDLE(Nothing, CellValue, cell_get_value(&cell));
    operand->is_loaded = 1;
    OK(Nothing);
}


Maybe_uint8_t predicate_apply(Row *row, Predicate predicate)
{
    HANDLE_NOTHING(uint8_t, operand_load(row, &predicate.left_operand))
    HANDLE_NOTHING(uint8_t, operand_load(row, &predicate.right_operand))
    if (predicate.left_operand.value.type != predicate.right_operand.value.type) EXCEPTION(uint8_t, "operands types does not match");

    RESULT(uint8_t, predicate.compare(predicate.left_operand.value, predicate.right_operand.value));
}

Predicate_LinkedList predicates_new(void)
{
    return Predicate_linked_list_new();
}

void predicates_add_predicate(Predicate_LinkedList *predicates, Predicate predicate)
{
    Predicate_linked_list_push_back(predicates, predicate);
}

Maybe_uint8_t predicates_apply(Row *row, Predicate_LinkedList *predicates)
{
    if ((row == NULL) | (predicates == NULL)) EXCEPTION(uint8_t, "null ptr");
    uint8_t result = 1;
    Predicate_LinkedListNode *it = predicates->head;
    while (it != NULL)
    {
        result &= HANDLE(uint8_t, uint8_t, predicate_apply(row, it->value));
        it = it->next;
    }
    RESULT(uint8_t, result);
}

void predicates_free(Predicate_LinkedList *predicates)
{
    Predicate_LinkedListNode *it = predicates->head;
    while (it != NULL)
    {
        predicate_free(&it->value);
        it = it->next;
    }
    Predicate_linked_list_free(predicates);
}