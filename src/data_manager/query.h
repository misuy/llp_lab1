#ifndef QUERY_H_
#define QUERY_H_

#include "transformation.h"
#include "data_iterator.h"
#include "table.h"

typedef enum QueryType
{
    SELECT,
    INSERT,
    DELETE,
    UPDATE,
} QueryType;


typedef struct SelectQuery
{
    DataIterator *it;
    Predicate_LinkedList predicates;
} SelectQuery;

typedef struct InsertQuery
{
    Table *table;
    Row_LinkedList rows;
} InsertQuery;

typedef struct DeleteQuery
{
    Table *table;
    Predicate_LinkedList predicates;
} DeleteQuery;

typedef struct UpdateQuery
{
    Table *table;
    Predicate_LinkedList predicates;
    Transformation_LinkedList transformations;
} UpdateQuery;

typedef struct Query
{
    QueryType type;
    union
    {
        SelectQuery select;
        InsertQuery insert;
        DeleteQuery delete;
        UpdateQuery update;
    };
} Query;



uint32_t query_execute(State *state, Query *query);
Query query_new(QueryType type);
void query_free(Query *query);

uint32_t select_query_execute(State *state, Query *query);
Query select_query_new(void);
void select_query_set_data_iterator(Query *query, DataIterator *it);
void select_query_set_predicates(Query *query, Predicate_LinkedList predicates);
void select_query_add_predicate(Query *query, Predicate predicate);

uint32_t insert_query_execute(State *state, Query *query);
Query insert_query_new(void);
void insert_query_set_table(Query *query, Table *table);
void insert_query_add_row(Query *query, Row row);
void insert_query_set_rows(Query *query, Row_LinkedList rows);

uint32_t delete_query_execute(State *state, Query *query);
Query delete_query_new(void);
void delete_query_set_table(Query *query, Table *table);
void delete_query_set_predicates(Query *query, Predicate_LinkedList predicates);
void delete_query_add_predicate(Query *query, Predicate predicate);

uint32_t update_query_execute(State *state, Query *query);
Query update_query_new(void);
void update_query_set_table(Query *query, Table *table);
void update_query_set_predicates(Query *query, Predicate_LinkedList predicates);
void update_query_add_predicate(Query *query, Predicate predicate);
void update_query_set_transformations(Query *query, Transformation_LinkedList transformations);
void update_query_add_transformation(Query *query, Transformation transformation);


#endif