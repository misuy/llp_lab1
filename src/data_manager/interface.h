#ifndef INTERFACE_H_
#define INTERFACE_H_

#include "table.h"
#include "predicate.h"
#include "transformation.h"
#include "query.h"

Table* table_open(State *, char *);
Table* table_create(State *, char *, ColumnSchema_LinkedList *);
void table_close(State *, Table *);

State storage_connect(char *name);
void storage_disconnect(State *state);

#endif