#include "transformation.h"

LINKED_LIST(Transformation);

CellValuePtr arg_ptr_new(ColumnType type, char *name)
{
    return (CellValuePtr) { .type = type, .name = name };
}

Transformation transformation_new(CellValuePtr arg_ptr, CellValue (*transform) (CellValue))
{
    return (Transformation) { .arg_ptr = arg_ptr, .transform = transform };
}

Transformation_LinkedList transformations_new(void)
{
    return Transformation_linked_list_new();
}

void transformations_add_transformation(Transformation_LinkedList *transformations, Transformation transformation)
{
    Transformation_linked_list_push_back(transformations, transformation);
}

void transformations_free(Transformation_LinkedList *transformations)
{
    Transformation_linked_list_free(transformations);
}

Maybe_Nothing transformation_apply(Transformation *transformation, Row *row)
{
    if ((transformation == NULL) | (row == NULL)) EXCEPTION(Nothing, "null ptr");
    Cell arg_cell = HANDLE(Nothing, Cell, row_get_cell_by_name(row, transformation->arg_ptr.name));
    CellValue arg = HANDLE(Nothing, CellValue, cell_get_value(&arg_cell));
    if (arg.type != transformation->arg_ptr.type) EXCEPTION(Nothing, "illegal arg type");
    CellValue transformed_arg = transformation->transform(arg);
    Cell transformed_arg_cell = HANDLE(Nothing, Cell, cell_from_value(&transformed_arg));
    HANDLE_NOTHING(Nothing, row_set_cell_by_name(row, transformed_arg_cell, transformation->arg_ptr.name));
    OK(Nothing);
}

Maybe_Nothing transformations_apply(Transformation_LinkedList *transformations, Row *row)
{
    if ((transformations == NULL) | (row == NULL)) EXCEPTION(Nothing, "null ptr");
    Transformation_LinkedListNode *it = transformations->head;
    while (it != NULL)
    {
        HANDLE_NOTHING(Nothing, transformation_apply(&it->value, row));
        it = it->next;
    }
    OK(Nothing);
}
