.PHONY: all clean build run

CC_FLAGS = -Werror -Wall -Wextra -Wmissing-prototypes -Wstrict-prototypes
CC = gcc
LD = ld
SRC_DIR = src
BUILD_DIR = build
DATA_MANAGER_BUILD_DIR = $(BUILD_DIR)/data_manager
TO_BUILD = \
	$(DATA_MANAGER_BUILD_DIR)/file.o \
	$(DATA_MANAGER_BUILD_DIR)/table.o \
	$(DATA_MANAGER_BUILD_DIR)/cell.o \
	$(DATA_MANAGER_BUILD_DIR)/raw_slot.o \
	$(DATA_MANAGER_BUILD_DIR)/slot.o \
	$(DATA_MANAGER_BUILD_DIR)/page.o \
	$(DATA_MANAGER_BUILD_DIR)/predicate.o \
	$(DATA_MANAGER_BUILD_DIR)/row.o \
	$(DATA_MANAGER_BUILD_DIR)/interface.o \
	$(DATA_MANAGER_BUILD_DIR)/query.o \
	$(DATA_MANAGER_BUILD_DIR)/transformation.o \
	$(DATA_MANAGER_BUILD_DIR)/data_iterator.o \
	$(BUILD_DIR)/test.o
TARGET = $(BUILD_DIR)/lab1

PYTHON3 = python3
TEST_DIR = test
TEST_SCRIPT = test.py
TEST_STORAGE_FILE = $(TEST_DIR)/test_storage_file
TEST_INSERT_RESULTS = $(TEST_DIR)/insert_results
TEST_DELETE_RESULTS = $(TEST_DIR)/delete_results
TEST_RAM_RESULTS = $(TEST_DIR)/ram_results



$(shell mkdir -p $(BUILD_DIR))
$(shell mkdir -p $(DATA_MANAGER_BUILD_DIR))
$(shell mkdir -p $(TEST_DIR))

all: build

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CC_FLAGS) -c -o $@ $^

$(TARGET): $(TO_BUILD)
	$(CC) -lm -o $@ $^

build: $(TARGET)

run: build
	./$(TARGET)

test: build $(TEST_SCRIPT)
	$(PYTHON3) $(TEST_SCRIPT) $(TARGET) $(TEST_STORAGE_FILE) $(TEST_INSERT_RESULTS) $(TEST_DELETE_RESULTS) $(TEST_RAM_RESULTS)

clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(TEST_DIR)