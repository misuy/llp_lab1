# Сборка
### Собрать:

`make build`

### Запустить:

`make run`

`lab1 [mode (1 -- 500 insert 400 delete test; 2 -- general tests)]`

`lab1 1 [storage_file_name] [insert_results_file_name] [delete_results_file_name]`

`lab1 2 [storage_file_name]`

### Протестировать:

`make test`

Запустить 500 вставок, 400 удалений тест (выполняется долго, есть готовые результаты в /5000test)
