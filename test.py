import subprocess
import sys
import os
import time
import psutil


target_path = sys.argv[1]

storage_file_path = sys.argv[2]
if os.path.exists(storage_file_path):
    os.remove(storage_file_path)

insert_results_path = sys.argv[3]
if os.path.exists(insert_results_path):
    os.remove(insert_results_path)

delete_results_path = sys.argv[4]
if os.path.exists(delete_results_path):
    os.remove(delete_results_path)

tester = subprocess.Popen([target_path, "1", storage_file_path, insert_results_path, delete_results_path])
tester_info = psutil.Process(tester.pid)

ram_results = []
while tester.poll() is None:
    ram_results.append(tester_info.memory_info().rss)
    time.sleep(0.1)
tester.kill()

print(ram_results)

ram_results_path = sys.argv[5]
if os.path.exists(ram_results_path):
    os.remove(ram_results_path)

ram_results_file = open(ram_results_path, "w")

for ram_result in ram_results:
    ram_results_file.write(str(ram_result))
    ram_results_file.write(',')

ram_results_file.close()
